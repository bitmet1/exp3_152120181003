/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#include "Circle.h"

	Circle::Circle(double r) {
		setR(r);
	}

	Circle::~Circle() {
	}

	void Circle::setR (double r) {
		this->r = r;
	}
	
	void Circle::setR(int r) {
		this->r = r;
	}

	double Circle::getR() const {
		return r;
	}

	double Circle::calculateCircumference() const {
		return PI * r * 2;
	}

	double Circle::calculateArea() {
		return PI * r * r;
	}
	bool Circle :: IsEqual(const Circle & circle4)
	{
		bool result;
		if (circle4.r == r)
		{
			result = true;
		}
		else
		{
			result = false;
		}
		return result;
	}

